require "json"

module Tenkai
  class Await
    def initialize(cluster : String, task_definition : String, region : String)
      @cluster         = cluster
      @task_definition = task_definition
      @region          = region
    end

    def run
      STDOUT.sync = true if STDOUT.tty?
      task_arn    = launch_task()
      last_status = nil
      loop do
        status = task_status(task_arn)
        if status == "STOPPED"
          print "\n" unless last_status.nil?
          puts "-> Task has completed."
          break
        elsif status != last_status
          print "\n" unless last_status.nil?
          last_status = status
          print "-> Waiting for task, current status is #{status}.."
        else
          print "."
        end
        sleep(2)
      end
    end

    def launch_task
      puts "-> Starting task #{@task_definition}..."
      response = Tenkai.cmd("aws", [
        "ecs", "run-task",
        "--region", @region,
        "--cluster", @cluster,
        "--task-definition", @task_definition,
        "--started-by", "tenkai/#{Tenkai.whoami}"
      ])
      parsed_response = JSON.parse(response)
      begin
        task_arn = parsed_response["tasks"][0]["taskArn"].as_s

        puts "-> Launched task"
        return task_arn
      rescue KeyError
        puts "Error: could not start task."
        puts response
        exit(1)
      end
    end

    def task_status(arn)
      response = Tenkai.cmd("aws", [
        "ecs", "describe-tasks",
        "--cluster", @cluster,
        "--region", @region,
        "--tasks", arn
      ])
      task = JSON.parse(response)["tasks"][0]["lastStatus"]
    end
  end
end
