module Tenkai
  class Maintenance
    def initialize(cluster : String, service : String, region : String, definition : String = "", maintenance_definition : String = "")
      @cluster                = cluster
      @service                = service
      @region                 = region
      @definition             = definition
      @maintenance_definition = maintenance_definition
    end
    def on!
      set_definition(@maintenance_definition)
      puts "-> Maintenance mode on!"
    end
    def off!
      set_definition(@definition)
      puts "-> Maintenance mode off!"
    end
    def set_definition(definition)
      Tenkai.cmd("aws", [
        "ecs", "update-service",
        "--cluster", @cluster,
        "--region", @region,
        "--service", @service,
        "--task-definition", definition
      ])
    end
  end
end
