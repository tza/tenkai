module Tenkai
  class Deploy
    def initialize(@region : String)
      login
    end
    def login
      Tenkai.cmd("docker", login_command.split(" ")[1..-1])
    end
    def push_image(image : String)
      imagename, tagname = split_image_name(image)
      target = "#{repository_host}/#{imagename}:#{tagname}"
      tag_image("#{image}:#{tagname}", target)
      return target
    end
    def all_image_versions(image : String)
      image_name = strip_repo_and_tags(image)
      response = Tenkai.cmd("aws", [
        "ecr", "describe-images",
        "--repository-name", image_name,
        "--region", @region
      ])
      ret = {} of String => Hash(String, JSON::Any)
      JSON.parse(response)["imageDetails"].as_a.map do |image|
        image = image.as_h
        if image.has_key?("imageTags")
          image["imageTags"].as_a.each do |tag|
            ret[tag.as_s] = image
          end
        end
      end.compact
      return ret
    end
    def strip_repo_and_tags(image : String)
      image.sub(/:[^:]*$/, "").sub(/^#{repository_host}\//,"")
    end
    def print_all_versions(image : String)
      version_list = all_image_versions(image).map do |tag, image|
        if image.has_key?("imageTags")
          tags = image["imageTags"].as_a.map(&.as_s)
          is_active = tags.includes?("active")
          {tag, is_active, tags - ["active", tag]}
        end
      end.compact.sort_by(&.[0])
      image_prefix = "#{repository_host}/#{strip_repo_and_tags(image)}"
      version_list.each do |tag, active, other_tags|
        next unless tag[0] == 'v'
        if active
          print "* "
        else
          print "  "
        end
        print "#{image_prefix}:#{tag}"
        if other_tags.any?
          puts " (other tags: #{other_tags.join(", ")})"
        else
          puts
        end
      end
    end
    def active_image_version(image : String)
      all_image_versions(image)["active"]?
    end
    def active_image_version_tag(image : String) : String?
      if image_info = active_image_version(image)
        return unless image_info.has_key?("imageTags")
        version_tags = image_info["imageTags"].as_a.map(&.as_s).select do |tag|
          tag[0] == 'v'
        end
        unless version_tags.empty?
          "#{repository_host}/#{image_info["repositoryName"]}:#{version_tags.last}"
        end
      end
    end
    def deploy(cluster : String, image : String, deployment_tag : String = "active")
      if initial_version = active_image_version_tag(image)
        puts "-> Active version is #{initial_version}"
      end
      if image.includes?(repository_host)
        remote_image = image
        unless remote_image.includes?(":")
          remote_image += ":latest"
        end
      else
        remote_image = push_image(image)
        versioned_image = remote_image.sub(/:[^:]+$/, ":#{make_version_tag}")
        tag_image(remote_image, versioned_image)
      end
      deploy_image = remote_image.sub(/:[^:]+$/, ":#{deployment_tag}")
      tag_image(remote_image, deploy_image)
      restart_services_using_image(cluster, deploy_image)
      puts "-> Deployed #{versioned_image}"
      if initial_version
        puts "-> To roll back, run:"
        puts "-> tenkai deploy #{cluster} #{initial_version}"
      end
    end
    def restart_services_using_image(cluster : String, image : String)
      util = Tenkai::Env.new(region: @region)
      Tenkai::Service.new(cluster: cluster, region: @region).tap do |services|
        services.all_services.select do |service_arn|
          service_info    = services.get_service_info(service_arn)
          definition_arn  = service_info["taskDefinition"].as_s
          definition_info = util.get_task_definition(definition_arn)
          matches         = definition_info["containerDefinitions"].as_a.any? do |container_definition|
            container_definition["image"] == image
          end
        end.tap do |services_to_restart|
          Tenkai::Restart.new(cluster: cluster, region: @region).run(services_to_restart)
        end
      end
    end
    def redeploy_service(arn : String)
      puts "-> Redeploying #{arn}"
    end
    def make_version_tag
      now = Time.utc
      "v#{now.to_s("%Y-%m-%d %H%M").sub(" ","T")}"
    end
    def tag_image(image, tag)
      puts "-> Tagging #{image} as #{tag}"
      Tenkai.cmd("docker", ["tag", image, tag])
      Tenkai.cmd("docker", ["push", tag])
    end
    def split_image_name(image : String)
      if image.includes?(":")
        imagename, tagname = image.split(":", 2)
      else
        imagename = image
        tagname   = "latest"
      end
      {imagename, tagname}
    end
    def login_command
      Tenkai.cmd("aws", [
        "ecr", "get-login",
        "--region", @region,
        "--no-include-email"
      ]).chomp
    end
    @repository_host : String?
    def repository_host
      @repository_host ||= begin
                             uri = login_command.split(" ").last
                             hostname = uri.split("://").last if uri
                             if uri && hostname
                               hostname
                             else
                               puts "Could not get repository information."
                               exit(1)
                             end
                           end
    end
  end
end
