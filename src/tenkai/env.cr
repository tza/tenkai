module Tenkai
  class Env
    def initialize(region : String, filters : Array(String) = [] of String)
      @region  = region
      @filters = filters
    end
    def print
      all_envs = {} of String => Hash(String,String)
      latest_matching_task_definitions.each do |arn|
        all_envs[arn] = get_env(arn)
      end
      common_env = get_common_env(all_envs)
      unless common_env.empty?
        print_env("Global", common_env)
      end
      all_envs.each do |arn, env|
        individual_env = remove_common_env(env, common_env)
        next if individual_env.empty?
        print_env(arn.split("/",2).last, individual_env)
      end
      return
    end
    def set_envs(env : Hash(String,String?), dryrun = false)
      updated = [] of String
      latest_matching_task_definitions.each do |arn|
        task_definition = get_task_definition(arn)
        new_env = extract_env(task_definition)
        env.each do |k,v|
          if v.nil?
            new_env.delete(k)
          else
            new_env[k] = v
          end
        end
        set_env(task_definition, new_env)
        update_task_definition(task_definition, dryrun)
        updated.push(arn.split("/",2)[1].split(":",2)[0])
      end
      puts "Updated:"
      updated.each do |family|
        puts "- #{family}"
      end
      print_env("Modified", env)
    end
    def set_env(task_definition, env : Hash(String,String))
      task_definition["containerDefinitions"].as_a.map(&.as_h).each do |container_definition|
        container_definition["environment"] = JSON::Any.new(env.map do |k, v|
          JSON::Any.new({ "name" => JSON::Any.new(k), "value" => JSON::Any.new(v) })
        end)
      end
    end
    def update_task_definition(task_definition, dryrun = false)
      args = [
        "ecs", "register-task-definition",
        "--family", task_definition["family"].as_s,
        "--container-definitions", task_definition["containerDefinitions"].to_json,
        "--region", @region
      ]
      { # TODO - other pass-through arguments
        "taskRoleArn" => "--task-role-arn",
        "cpu"         => "--cpu",
        "memory"      => "--memory"
      }.each do |key, option|
        if task_definition.has_key?(key)
          args += [option, task_definition[key].as_s]
        end
      end
      Tenkai.cmd("aws", args) unless dryrun
    end
    def get_common_env(envs : Hash(String, Hash(String,String)))
      common_env  = {} of String => String
      return common_env if envs.empty?
      common_keys = envs.map {|_, env| env.keys }.reduce {|x, y| x & y}
      common_keys.each do |k|
        common_values = envs.map {|_, env| env[k] }.uniq
        if common_values.size == 1
          common_env[k] = common_values[0]
        end
      end
      return common_env
    end
    def remove_common_env(env : Hash(String, String), common : Hash(String, String))
      env.map do |k,v|
        if common.has_key?(k) && common[k] == v
          nil
        else
          [k, v]
        end
      end.compact.to_h
    end
    def print_env(title : String, env : Hash(String, String?))
      puts "="*80
      puts "-> #{title} insecure environment:"
      puts "="*80
      return if env.keys.empty?
      max_key_size = env.keys.sort_by {|x| x.size}.last.size
      env.keys.sort.each do |key|
        print key
        print ":"
        print " "*(max_key_size - key.size + 1)
        puts env[key]
      end
    end
    def get_env(arn)
      task_definition = get_task_definition(arn)
      extract_env(task_definition)
    end
    def extract_env(task_definition : Hash(String, JSON::Any))
      ret = {} of String => String
      task_definition["containerDefinitions"].as_a.each do |container_definition|
        container_definition["environment"].as_a.each do |var|
          ret[var["name"].as_s] = var["value"].as_s
        end
      end
      return ret
    end
    def matching_task_definitions : Hash(String, Array(String))
      definitions = all_task_definitions.select do |service|
        @filters.empty? || @filters.any? do |filter|
          matches?(filter, service)
        end
      end
      # Group by family
      by_family = definitions.group_by do |definition|
        definition.split("/",2).last.split(":").first
      end
      by_family.each do |_, definitions|
        definitions.sort_by! {|definition| definition.split(":").last.to_i }
      end
      return by_family
    end
    def latest_matching_task_definitions
      matching_task_definitions.values.map {|x| x.last}
    end
    def matches?(filter : String, str : String)
      str.includes?(filter)
    end
    def all_task_definitions
      response = Tenkai.cmd("aws", [
        "ecs", "list-task-definitions",
        "--region", @region
      ])
      JSON.parse(response)["taskDefinitionArns"].as_a.map do |arn|
        arn.as_s
      end
    end

    def get_task_definition(arn)
      response = Tenkai.cmd("aws", [
        "ecs", "describe-task-definition",
        "--task-definition", arn,
        "--region", @region
      ])
      JSON.parse(response)["taskDefinition"].as_h
    end
  end
end
