module Tenkai
  class PS
    alias TaskInfo = NamedTuple(task_id: String, definition: String, started_by: String)
    def initialize(region : String, cluster : String)
      @region  = region
      @cluster = cluster
    end
    def print_tasks(task_info = get_all_task_info)
      if task_info.empty?
        puts "No matching tasks running."
        return
      end
      max_definition_length = task_info.map do |info|
        info[:definition].size
      end.max
      puts "="*100
      print "Definition"
      print " "*(max_definition_length - "Definition".size)
      print "    "
      print "Task ID"
      print " "*(36 - "Task ID".size)
      print "    "
      puts "Started by"
      puts "="*100
      task_info.sort_by {|x| x[:definition]}.each do |task_info|
        padding = max_definition_length - task_info[:definition].size
        puts "%s#{" "*padding}    %s    %s" % [
          task_info[:definition], task_info[:task_id], task_info[:started_by]
        ]
      end
    end
    def kill_tasks_matching(matches : Array(String), dryrun : Bool = false)
      filtered = get_all_task_info.sort_by(&.[:definition]).select do |task|
        matches.empty? || matches.any? do |match|
          matches?(task, match)
        end
      end
      filtered.each do |info|
        kill_task(info) unless dryrun
      end
      if dryrun
        puts "Would kill the following tasks:"
      else
        puts "Killed the following tasks:"
      end
      print_tasks(filtered)
    end
    def kill_task(info : TaskInfo)
      Tenkai.cmd("aws", [
        "ecs", "stop-task",
        "--cluster", @cluster,
        "--region", @region,
        "--task", info[:task_id],
        "--reason", "Manually killed by #{Tenkai.whoami}"
      ])
    end
    def matches?(info : TaskInfo, str : String)
      info.each_value do |prop|
        return true if prop.includes?(str)
      end
    end
    def get_all_task_info
      ret = [] of TaskInfo
      list_tasks.each_slice(100) do |task_arns|
        ret += get_task_info(task_arns)
      end
      ret
    end
    def list_tasks : Array(String)
      response = Tenkai.cmd("aws", [
        "ecs", "list-tasks",
        "--cluster", @cluster,
        "--region", @region
      ])
      JSON.parse(response)["taskArns"].as_a.map(&.as_s)
    end
    def get_task_info(arns : Array(String))
      json = JSON.build do |json|
        json.object do
          json.field "cluster", @cluster
          json.field "tasks", arns.map(&.split("/")).map(&.last)
        end
      end
      args = [
        "ecs", "describe-tasks",
        "--region", @region,
        "--cli-input-json", json
      ]
      response = Tenkai.cmd("aws", args)
      JSON.parse(response)["tasks"].as_a.map do |task_info|
        started_by = if task_info.as_h.has_key?("startedBy")
                       task_info["startedBy"].as_s
                     else
                       ""
                     end
        {
          task_id:    task_info["taskArn"].as_s.split("/").last,
          definition: task_info["taskDefinitionArn"].as_s.split("/").last,
          started_by: started_by
        }
      end
    end
  end
end
